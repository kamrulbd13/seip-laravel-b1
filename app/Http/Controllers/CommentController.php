<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use App\Notifications\NewComment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {

        $product = Product::findOrFail($id);
        $comment = $product->comments()->create([
            'body' => $request->body,
            'created_by' => auth()->user()->id
        ]);

        $users = User::where('role_id', 1)->get();
        foreach ($users as $user) {
            $user->notify(new NewComment($comment));
        }

        return redirect()->back();
    }
}
